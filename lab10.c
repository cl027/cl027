#include<stdio.h>
#include<math.h>
int main()
{
float a,b,c;
float root1,root2,imaginary;
float discriminant;
printf("Enter values of a,b,c in the quadratic equation (ax^2+bx+c):");
scanf("%f %f %f",&a,&b,&c);
discriminant=(b*b)-(4*a*c);
if(discriminant>0)
{
root1=(-b+sqrt(discriminant))/(2*a);
root2=(-b-sqrt(discriminant))/(2*a);
printf("\nTwo real and distinct roots exist: %f and %f",root1,root2);
}
else
if(discriminant==0)
{
root1=root2=-b/(2*a);
printf("\nTwo equal and real roots exist: %f and %f",root1,root2);
}
else
if(discriminant<0)
{
root1=root2=-b/(2*a);
imaginary=(sqrt(-discriminant))/(2*a);
printf("\nTwo distinct complex roots exist: %f+i%f and %f-i%f",root1,imaginary,root2,imaginary);
}
return 0;
}
